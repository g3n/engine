module gitlab.com/g3n/engine/v1.0.0

go 1.13

require (
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20210410170116-ea3d685f79fb
	github.com/goki/freetype v0.0.0-20220119013949-7a161fd3728c
	golang.org/x/image v0.0.0-20210607152325-775e3b0c77b9
	gopkg.in/yaml.v2 v2.4.0
)
