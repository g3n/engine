package core

import (
	"gitlab.com/g3n/engine/util/logger"
)

// Package logger
var log = logger.New("CORE", logger.Default)
